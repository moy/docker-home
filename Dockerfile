FROM ubuntu:17.10
MAINTAINER Matthieu Moy <git@matthieu-moy.fr>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -qy

# Needed to get proper english messages without errors
RUN apt-get install -qy locales
RUN locale-gen en_US.UTF-8

# Network basic utilities
RUN apt-get install -qy iputils-ping iproute2 iputils-tracepath wget bind9-host

# Compilation basic utilities
RUN apt-get install -qy build-essential cmake

# How can you live without this? (ncurses-term is needed to run a shell with TERM=eterm)
RUN apt-get install -qy zsh git vim emacs ncurses-term

# Python
RUN apt-get install -qy python3 python3-pip

# Debug tools
RUN apt-get install -qy gdb valgrind

# Python tools
RUN pip3 install pytest

# ANTLR
RUN pip3 install antlr4-python3-runtime

# Java
RUN apt-get install -qy openjdk-9-jdk
