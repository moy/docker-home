# Docker HOME

Simple script to run some commands in a sandbox:

* `/` is obtained from a Docker image (any image can do)
* `$HOME` is mounted read-only within the container, from the actual
  `$HOME`.
* `$PWD` is mounted read-write from the host to the container.

A new shell is opened within the container, in the directory where the
container was launched.

In other works, commands you run inside the container cannot modify
files outside the directory where you started the container.

## Usage

### Run the default image

Once:

```
docker build . -t docker-ubuntu
```

```
./docker-home
```

### Run a user-specified image

```
./docker-home mugen/ubuntu-build-essential
```
